## Vagrant Batfish PoC:

From the usage perspective, Batfish consists of two major elements:

- Batfish application, which is delivered as a Docker container listening to 3 ports: 8888/TCP, 9996/TCP and 9997/TCP. This part is responsible for analysis.

- Pybatfish, which is a Python library to interact with the application. This Library is used within your Python script to upload the configuration files, ask the questions, receive answers and structure the response data.

On this setup we leveraged Podman to bring up a batfish container and used python scripts from [this](https://github.com/batfish/pybatfish/blob/master/jupyter_notebooks/Validating%20Configuration%20Settings.ipynb) scenario to validate NTP setting against configuration files on the border routers.

Sample config files and python scripts are located in "/vagrant" directory.

Batfish Pod will be automaticly started once VM comes up!

To check the status, start and stop Batfish pod:
```
podman ps -a
podman start batfish
podman stop batfish
```

To analyze and validate NTP settings against configuration files: 
```
python3 /vagrant/batfish_ci_custom_check/01_ntp_configured_NTP_servers.py
python3 /vagrant/batfish_ci_custom_check/02_ntp_configured_no_NTP.py
python3 /vagrant/batfish_ci_custom_check/03_ntp_reference_set_has_configured.py
python3 /vagrant/batfish_ci_custom_check/04_ntp_reference_set_has_NOT_configured.py
python3 /vagrant/batfish_ci_custom_check/05_ntp_complete_view_extra_and_missing_servers.py
python3 /vagrant/batfish_ci_custom_check/06_ntp_match_NTP_Servers_with_per-node_database.py
```