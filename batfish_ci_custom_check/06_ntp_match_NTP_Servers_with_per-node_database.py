# Import packages
#######################################################################################
# this needs Jinja and ipython (install both with pip3)
# Importing required libraries, setting up logging, and loading questions
import os
import logging
import random  # noqa: F401

import pandas as pd
from IPython.display import display
from pandas.io.formats.style import Styler

from pybatfish.client.session import Session  # noqa: F401

# noinspection PyUnresolvedReferences
from pybatfish.datamodel import Edge, Interface  # noqa: F401
from pybatfish.datamodel.answer import TableAnswer
from pybatfish.datamodel.flow import HeaderConstraints, PathConstraints  # noqa: F401
from pybatfish.datamodel.route import BgpRoute  # noqa: F401
from pybatfish.util import get_html

# Configure all pybatfish loggers to use WARN level
logging.getLogger("pybatfish").setLevel(logging.WARN)

pd.set_option("display.max_colwidth", None)
pd.set_option("display.max_columns", None)
# Prevent rendering text between '$' as MathJax expressions
pd.set_option("display.html.use_mathjax", False)

# UUID for CSS styles used by pandas styler.
# Keeps our notebook HTML deterministic when displaying dataframes
_STYLE_UUID = "pybfstyle"


class MyStyler(Styler):
    """A custom styler for displaying DataFrames in HTML"""

    def __repr__(self):
        return repr(self.data)


def show(df):
    """
    Displays a dataframe as HTML table.

    Replaces newlines and double-spaces in the input with HTML markup, and
    left-aligns the text.
    """
    if isinstance(df, TableAnswer):
        df = df.frame()

    # workaround for Pandas bug in Python 2.7 for empty frames
    if not isinstance(df, pd.DataFrame) or df.size == 0:
        display(df)
        return
    display(
        MyStyler(df)
        .set_uuid(_STYLE_UUID)
        .format(get_html)
        .set_properties(**{"text-align": "left", "vertical-align": "top"})
    )

########################################################################################
bf = Session(host="localhost")

# Initialize a network and snapshot
NETWORK_NAME = "example_network"
SNAPSHOT_NAME = "example_snapshot"

SNAPSHOT_PATH = "/vagrant/batfish_ci_custom_check/snapshot/border/"

bf.set_network(NETWORK_NAME)
bf.init_snapshot(SNAPSHOT_PATH, name=SNAPSHOT_NAME, overwrite=True)

# Variable for output dir
output_dir = "/vagrant/batfish_ci_custom_check/output"

# Mock reference-node-data, presumably taken from an external database
database = {'as1border1': {'NTP_Servers': ['23.23.23.23'],
                           'DNS_Servers': ['1.1.1.1']},
            'as1border2': {'NTP_Servers': ['23.23.23.23'],
                           'DNS_Servers': ['1.1.1.1']},
            'as2border1': {'NTP_Servers': ['18.18.18.18', '23.23.23.23'],
                           'DNS_Servers': ['2.2.2.2']},
            'as2border2': {'NTP_Servers': ['18.18.18.18'],
                           'DNS_Servers': ['1.1.1.1']},
            'as3border1': {'NTP_Servers': ['18.18.18.18', '23.23.23.23'],
                           'DNS_Servers': ['2.2.2.2']},
            'as3border2': {'NTP_Servers': ['18.18.18.18', '23.23.23.23'],
                           'DNS_Servers': ['2.2.2.2']},
            }

# Set the property that we want to extract
COL_NAME = "NTP_Servers"

# Extract NTP servers for all routers with 'border' in their name
node_props = bf.q.nodeProperties(
    nodes="/border/", 
    properties=COL_NAME).answer().frame()
node_props

# Transpose database data so each node has its own row
database_df = pd.DataFrame(data=database).transpose()

# Index on node for easier comparison
df_node_props = node_props.set_index('Node')

# Select only columns present in node_props (get rid of the extra dns-servers column)
df_db_node_props = database_df[df_node_props.columns].copy()

# Convert server lists into sets to support arithmetic below
df_node_props[COL_NAME] = df_node_props[COL_NAME].apply(set)
df_db_node_props[COL_NAME] = df_db_node_props[COL_NAME].apply(set)

# Figure out what servers are in the configs but not the database and vice versa
missing_servers = (df_db_node_props - df_node_props).rename(
    columns={COL_NAME: 'missing-{}'.format(COL_NAME)})
extra_servers = (df_node_props - df_db_node_props).rename(
    columns={COL_NAME: 'extra-{}'.format(COL_NAME)})
result = pd.concat([missing_servers, extra_servers], axis=1, sort=False)
result

print(result)

# Saving output
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

result.to_csv(f"{output_dir}/06_ntp_match_NTP_Servers_with_per-node_database.csv")