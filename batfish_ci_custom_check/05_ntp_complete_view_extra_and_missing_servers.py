# Import packages
#######################################################################################
# this needs Jinja and ipython (install both with pip3)
# Importing required libraries, setting up logging, and loading questions
import os
import logging
import random  # noqa: F401

import pandas as pd
from IPython.display import display
from pandas.io.formats.style import Styler

from pybatfish.client.session import Session  # noqa: F401

# noinspection PyUnresolvedReferences
from pybatfish.datamodel import Edge, Interface  # noqa: F401
from pybatfish.datamodel.answer import TableAnswer
from pybatfish.datamodel.flow import HeaderConstraints, PathConstraints  # noqa: F401
from pybatfish.datamodel.route import BgpRoute  # noqa: F401
from pybatfish.util import get_html

# Configure all pybatfish loggers to use WARN level
logging.getLogger("pybatfish").setLevel(logging.WARN)

pd.set_option("display.max_colwidth", None)
pd.set_option("display.max_columns", None)
# Prevent rendering text between '$' as MathJax expressions
pd.set_option("display.html.use_mathjax", False)

# UUID for CSS styles used by pandas styler.
# Keeps our notebook HTML deterministic when displaying dataframes
_STYLE_UUID = "pybfstyle"


class MyStyler(Styler):
    """A custom styler for displaying DataFrames in HTML"""

    def __repr__(self):
        return repr(self.data)


def show(df):
    """
    Displays a dataframe as HTML table.

    Replaces newlines and double-spaces in the input with HTML markup, and
    left-aligns the text.
    """
    if isinstance(df, TableAnswer):
        df = df.frame()

    # workaround for Pandas bug in Python 2.7 for empty frames
    if not isinstance(df, pd.DataFrame) or df.size == 0:
        display(df)
        return
    display(
        MyStyler(df)
        .set_uuid(_STYLE_UUID)
        .format(get_html)
        .set_properties(**{"text-align": "left", "vertical-align": "top"})
    )

########################################################################################
bf = Session(host="localhost")

# Initialize a network and snapshot
NETWORK_NAME = "example_network"
SNAPSHOT_NAME = "example_snapshot"

SNAPSHOT_PATH = "/vagrant/batfish_ci_custom_check/snapshot/border/"

bf.set_network(NETWORK_NAME)
bf.init_snapshot(SNAPSHOT_PATH, name=SNAPSHOT_NAME, overwrite=True)

# Variable for output dir
output_dir = "/vagrant/batfish_ci_custom_check/output"

# Set the property that we want to extract
COL_NAME = "NTP_Servers"

# Extract NTP servers for all routers with 'border' in their name

#print("ANALYSIS // NTP()")
node_props = bf.q.nodeProperties(
    nodes="/border/", 
    properties=COL_NAME).answer().frame()
node_props
#print(node_props)

# Define the reference set of NTP servers
ref_ntp_servers = set(["23.23.23.23","18.18.18.18"])

# Find extra and missing servers at each node
ns_extra = node_props[COL_NAME].map(lambda x: set(x) - ref_ntp_servers)
ns_missing = node_props[COL_NAME].map(lambda x: ref_ntp_servers - set(x))

# Join these columns up with the node columns for a complete view
diff_df = pd.concat([node_props["Node"],
                     ns_extra.rename('extra-{}'.format(COL_NAME)),
                     ns_missing.rename('missing-{}'.format(COL_NAME))],
                    axis=1)
diff_df

print(diff_df)

# Saving output
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

diff_df.to_csv(f"{output_dir}/05_ntp_complete_view_extra_and_missing_servers.csv")