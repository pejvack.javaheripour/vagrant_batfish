#!/bin/bash
echo "@@@@@@@@@@ Starting Batfish POD... @@@@@@@@@@"
podman start batfish

if [ $? -eq 0 ]
then
	echo "Batfish POD has been started successfully!"	
else
	echo "Batfish POD has been faild to start!"
fi
