#!/bin/bash

# Adding EPEL & IUS Reposifity:
echo "@@@@@@@@@@ Adding EPEL & IUS Reposifity! @@@@@@@@@@"
sudo yum -y install epel-release
sudo rpm -ivh https://repo.ius.io/ius-release-el7.rpm

# Assigning "sub-uids" and "sub-gids" to vagrant user.
# For more information: https://www.funtoo.org/LXD/What_are_subuids_and_subgids%3F
echo "@@@@@@@@@@ Assigning sub-uids and sub-gids to vagrant user! @@@@@@@@@@"
sudo sysctl user.max_user_namespaces=15000
sudo usermod --add-subuids 200000-201000 --add-subgids 200000-201000 vagrant

# Making the user.max_user_namespaces configuration permanent: 
echo "@@@@@@@@@@ Making (user.max_user_namespaces) configuration permanent! @@@@@@@@@@"
sudo touch /etc/sysctl.d/98-userns.conf
sudo chown vagrant:root /etc/sysctl.d/98-userns.conf
echo user.max_user_namespaces=15000 >> /etc/sysctl.d/98-userns.conf
sudo sysctl -p

# Installing Podman:
echo "@@@@@@@@@@ Installing Podman! @@@@@@@@@@"
sudo yum -y install podman

# Pulling Batfish image in Podman:
echo "@@@@@@@@@@ Pulling Batfish image! @@@@@@@@@@"
podman pull batfish/allinone

# Creating Batfish container:
echo "@@@@@@@@@@ Creating Batfish container! @@@@@@@@@@"
podman run -d --name batfish -v batfish-data:/data -p 8888:8888 -p 9997:9997 -p 9996:9996 batfish/allinone

# Installing PIP3:
echo "@@@@@@@@@@ Installing PIP3! @@@@@@@@@@"
sudo yum install python3-pip -y

# Installing Pybatfish:
echo "@@@@@@@@@@ Installing Pybatfish & ipython & sudo python3 -m pip install Jinja2 @@@@@@@@@@"
sudo python3 -m pip install --upgrade pybatfish
sudo python3 -m pip install ipython
sudo python3 -m pip install Jinja2

# Changing python version on per user basis:
echo "@@@@@@@@@@ Changing python version on per user basis! @@@@@@@@@@"
alias python='/bin/python3.6'
source ~/.bashrc

